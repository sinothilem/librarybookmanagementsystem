﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementApi;
using BookManagementApi.Interfaces;
using BookManagementApiTests.Helpers;
using Nancy.Testing;

namespace BookManagementApiTests.Builders
{
    public class EndpointBuilder
    {
        private Browser browser;
        private Task<BrowserResponse> result;

        public EndpointBuilder(IBookRepository bookRepository)
        {
            browser = new Browser(with =>
            {
                with.Module<BookModule>();
                with.Dependency(bookRepository);
            });
        }

        public EndpointBuilder GetAll()
        {
            var fakeBookRepository = new FakeBookRepository();
            result = browser.Get("/book", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
                with.JsonBody(fakeBookRepository.GetAll());
            });

            return this;
        }

        public EndpointBuilder GetById(int id)
        {
            var fakeBookRepository = new FakeBookRepository();

            result = browser.Get($"/book/{id}", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
                with.JsonBody(fakeBookRepository.GetByID(id));
            });

            return this;
        }

        public EndpointBuilder Post(BookModel request)
        {
            result = browser.Post("/book", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
                with.JsonBody(request);
            });

            return this;
        }

        public EndpointBuilder Put(int id, BookModel request)
        {
            result = browser.Put($"/book/{id}", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
                with.JsonBody(request);
            });

            return this;
        }

        public EndpointBuilder Delete(int id)
        {
            result = browser.Delete($"/book/{id}", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
                with.JsonBody(id);
            });

            return this;
        }

        public Task<BrowserResponse> Build()
        {
            return result;
        }
    }
}
