﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementApi;

namespace BookManagementApiTests.Builders
{
    public class BookManagerBuilder
    {
        private BookModel bookModel;
        public BookManagerBuilder()
        {
            bookModel = new BookModel();
        }

        public BookManagerBuilder WithBook()
        {
            bookModel.BookId = 1;
            bookModel.Title = "Monday";
            bookModel.CategoryName = "Music";
            bookModel.PublishDate = new DateTime(2015, 12, 25);
            bookModel.PublisherName = "SA Home Loans";
            bookModel.AuthorName = "Mark brown";

            return this;
        }

        public BookModel Build()
        {
            return bookModel;
        }
    }
}
