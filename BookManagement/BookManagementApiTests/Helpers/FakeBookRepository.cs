﻿using BookManagementApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementApi;
using BookManagementApi.Models;

namespace BookManagementApiTests.Helpers
{
    public class FakeBookRepository 
    {
        public IEnumerable<BookModel> GetAll()
        {
            return new List<BookModel>(new[]
            {
                new BookModel()
                {
                    BookId = 1,
                    Title = "Dreams",
                    CategoryName = "Cook Book",
                    AuthorName = "Zazo",
                    PublishDate = new DateTime(2015, 12, 25),
                    PublisherName = "SA Home Loans"
                },
                new BookModel()
                {
                    BookId = 2,
                    Title = "I Love Cooking",
                    CategoryName = "Cook Book",
                    AuthorName = "Sinothile",
                    PublishDate = new DateTime(2015, 12, 25),
                    PublisherName = "SA Home Loans"
                },

            });
        }

        public void Add(BookModel book)
        {

        }

        public BookModel GetByID(int id)
        {
            return new BookModel()
            {
                BookId = 5,
                Title = "Fire",
                CategoryName = "Movies",
                AuthorName = "Shaun",
                PublishDate = new DateTime(2015, 12, 25),
                PublisherName = "SA Home Loans"
            };
        }

        public void Delete(int id)
        {

        }

        public void DeleteAuthor(int id)
        {

        }

        public void Update(BookModel book)
        {

        }

        public void UpdateAuthor(AuthorModel book)
        {

        }
        public void AddCategory(CategoryModel book)
        {
                
        }   
        public void UpdateCategory(CategoryModel book)
        {

        }
        public void AddAuthor(AuthorModel book)
        {

        }

        public IEnumerable<BookModel> GetCategories()
        {
            return new List<BookModel>(new[]
            {
                new BookModel()
                {
                    BookId = 1,
                    Title = "Dreams",
                    CategoryName = "Cook Book",
                    AuthorName = "Zazo",
                    PublishDate = new DateTime(2015, 12, 25),
                    PublisherName = "SA Home Loans"
                },
                new BookModel()
                {
                    BookId = 2,
                    Title = "I Love Cooking",
                    CategoryName = "Cook Book",
                    AuthorName = "Sinothile",
                    PublishDate = new DateTime(2015, 12, 25),
                    PublisherName = "SA Home Loans"
                },

            });

        }

        public IEnumerable<AuthorModel> GetAuthors()
        {
            return new List<AuthorModel>(new[]
            {
                new AuthorModel()
                {
                    AuthorName = "Zazo"
                },
                new AuthorModel()
                {
                    AuthorName = "Sinothile"
                },

            });

        }

        public IEnumerable<BookModel> GetPublishers()
        {
            return new List<BookModel>(new[]
            {
                new BookModel()
                {
                    BookId = 1,
                    Title = "",
                    CategoryName = "",
                    AuthorName = "",
                    PublishDate = new DateTime(),
                    PublisherName = "SA Home Loans"
                },
                new BookModel()
                {
                    BookId = 2,
                    Title = "",
                    CategoryName = "",
                    AuthorName = "",
                    PublishDate = new DateTime(),
                    PublisherName = "SA Home Loans"
                },

            });

        }
    }
}
