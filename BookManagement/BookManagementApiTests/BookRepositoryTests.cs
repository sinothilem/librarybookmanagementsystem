﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementApi;
using BookManagementApiTests.Builders;
using BookManagementApiTests.Helpers;
using FluentAssertions;
using Nancy.ViewEngines;
using NSubstitute;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace BookManagementApiTests
{
    [TestFixture]
    public class BookRepositoryTests
    {
        [Test]
        public void GetAll_ShouldReturnAllBooks()
        {
            // Arrange  
            var fakeBookRepository = new FakeBookRepository();

            // Act
            var result = fakeBookRepository.GetAll();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.AreEqual(result.Count(), 2);
        }

        [Test]
        public void GetById_ShouldReturnABookById()
        {
            // Arrange      
            var fakeBookRepository = new FakeBookRepository();
            var id = 5;

            // Act
            var result = fakeBookRepository.GetByID(id);

            // Assert
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Add_ShouldAddABookInDatabase()
        {
            // Arrange      
            var fakeBookRepository = new FakeBookRepository();
            var book = new BookManagerBuilder()
                        .WithBook()
                        .Build();
            // Act
            fakeBookRepository.Add(book);

            // Assert
            Assert.That(book, Is.Not.Null);
        }
    }
}
