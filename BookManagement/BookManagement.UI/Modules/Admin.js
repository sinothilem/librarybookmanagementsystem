var app = angular.module('MyAdminApp', [])
    app.controller('MyAdminController', function ($scope, $http, $window) {
        
        $scope.GetAuthors = function () {   
            var post = $http({
                    method: "GET",
                    url: "http://localhost:80/book/authors",
                    dataType: 'json',
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (data) {
                  $scope.authors = data.data;
                });
            };

            $scope.GetCategories = function () {   
                var post = $http({
                        method: "GET",
                        url: "http://localhost:80/book/categories",
                        dataType: 'json',
                        headers: { 
                            "Content-Type": "application/json" }
                    })
                    .then(function (data) {
                      $scope.categories = data.data;
                    });
                };

                $scope.GetPublishers = function () {   
                    var post = $http({
                            method: "GET",
                            url: "http://localhost:80/book/publishers",
                            dataType: 'json',
                            headers: { 
                                "Content-Type": "application/json" }
                        })
                        .then(function (data) {
                          $scope.publishers = data.data;
                        });
                    };

            $scope.AddAuthors = function () {

                let author = 
                { 
                    authorName: $scope.authorName,
                    dateOfBirth: new Date($scope.dateOfBirth),
                };
    
                var post = $http({
                    method: "POST",
                    url: "http://localhost:80/book/author/",
                    dataType: 'json',
                    data: author,
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (response) {
                    toastr.success("Successfully Added");
                    $scope.repos = response.data;
                    $scope.GetAuthors();
                    $scope.authorName = null;
                    $scope.dateOfBirth = null;
                });
            };

            $scope.AddCategories = function () {

                let categories = 
                { 
                    categoryName: $scope.categoryName,
                };
    
                var post = $http({
                    method: "POST",
                    url: "http://localhost:80/book/category/",
                    dataType: 'json',
                    data: categories,
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (response) {
                    toastr.success("Successfully Added");
                    $scope.repos = response.data;
                    $scope.GetCategories();
                    $scope.categoryId = null;
                    $scope.categoryName = null;
                });
            };

            $scope.AddPublishers = function () {

                let publisher = 
                { 
                    publisherName: $scope.publisherName,
                };
    
                var post = $http({
                    method: "POST",
                    url: "http://localhost:80/book/publisher/",
                    dataType: 'json',
                    data: publisher,
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (response) {
                    toastr.success("Successfully Added");
                    $scope.repos = response.data;
                    $scope.GetPublishers();
                    $scope.publisherName = null;
                });
            };


            $scope.UpdateAuthors = function () {

                let category = 
                { 
                    categoryId: $scope.categoryId,
                    categoryName: $scope.categoryName
                };
    
                var post = $http({
                    method: "PUT",
                    url: "http://localhost:80/book/category/" + category.categoryId,
                    dataType: 'json',
                    data: category,
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (response) {
                    toastr.success("Successfully Updated");
                    $scope.repos = response.data;
                    $scope.GetCategories();
                    $scope.categoryId = null;
                    $scope.categoryName = null;
                });
            };

            $scope.UpdateCategories = function () {

                let categories = 
                { 
                    authorId: $scope.id,
                    authorName: $scope.authorName,
                    dateOfBirth: new Date($scope.dateOfBirth),
                };
    
                var post = $http({
                    method: "PUT",
                    url: "http://localhost:80/book/author/" + author.authorId,
                    dataType: 'json',
                    data: author,
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (response) {
                    toastr.success("Successfully Updated");
                    $scope.repos = response.data;
                    $scope.GetAuthors();
                    $scope.id = null;
                    $scope.authorName = null;
                    $scope.dateOfBirth = null;
                });
            };

            $scope.DeleteAuthors = function () {

                $scope.showconfirmbox();
                
               if($scope.result == "Yes"){
                let author = 
                {authorId: $scope.id,};
    
                var post = $http({
                    method: "DELETE",
                    url: "http://localhost:80/book/author/"+ author.authorId,
                    dataType: 'json',
                    data: author,
                    headers: { 
                        "Content-Type": "application/json" }
                }).finally(function (response) {
                    $scope.GetAuthors();
                    toastr.success("Successfully Deleted");
                    $scope.id = null;
                    $scope.title = null;
                    $scope.category = null;
                    $scope.author = null;
                    $scope.publisher = null;
                    $scope.publishDate = null;
    
                    $scope.isDisabled = false;
                 });
               }
                
            };

            
            $scope.DeleteCategories = function () {

                $scope.showconfirmbox();
                
               if($scope.result == "Yes"){
                let categories = 
                {categoriesId: $scope.categoriesId};
    
                var post = $http({
                    method: "DELETE",
                    url: "http://localhost:80/book/categories/"+ categories.categoriesId,
                    dataType: 'json',
                    data: categories,
                    headers: { 
                        "Content-Type": "application/json" }
                }).finally(function (response) {
                    $scope.GetCategories();
                    toastr.success("Successfully Deleted");
                    $scope.categoriesId = null;
                    $scope.categoryName = null;
    
                    $scope.isDisabled = false;
                 });
               }
                
            };

            $scope.selectAuthors = function(e) {

                var table = document.getElementsByTagName("table")[0];
                var tbody = table.getElementsByTagName("tbody")[0];
                let data = [];
                tbody.onclick = function (e) {
                e = e || window.event;
                target = e.srcElement || e.target; 
                while (target && target.nodeName !== "TR") {
                target = target.parentNode;
                }
                
                if (target) {
                var cells = target.getElementsByTagName("td");
                for (var i = 0; i < cells.length; i++) {
                data.push(cells[i].innerHTML);
                $scope.isDisabled = true;
               } 
               $scope.id = data[0];
               $scope.authorName = data[1];
              }
            };
         };

         $scope.selectCategories = function(e) {

            var table = document.getElementsByTagName("table")[0];
            var tbody = table.getElementsByTagName("tbody")[0];
            let data = [];
            tbody.onclick = function (e) {
            e = e || window.event;
            target = e.srcElement || e.target; 
            while (target && target.nodeName !== "TR") {
            target = target.parentNode;
            }
            
            if (target) {
            var cells = target.getElementsByTagName("td");
            for (var i = 0; i < cells.length; i++) {
            data.push(cells[i].innerHTML);
            $scope.isDisabled = true;
           } 
           $scope.categoryiId = data[0];
           $scope.categoryName = data[1];
          }
        };
     };

            $scope.showconfirmbox = function () {
                if ($window.confirm("Are you Sure?"))
                $scope.result = "Yes";
                else
                $scope.result = "No";
                }
     
        $scope.GetCategories();
        $scope.GetAuthors();
        $scope.GetPublishers();
       
});