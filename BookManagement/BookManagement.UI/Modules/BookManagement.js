var app = angular.module('MyApp', [])
    app.controller('MyController', function ($scope, $http, $window, $element) {
        
        $scope.isDisabled = false;

        $scope.GetBooks = function () {   
        var post = $http({
                method: "GET",
                url: "http://localhost:80/book/",
                dataType: 'json',
                headers: { 
                    "Content-Type": "application/json" }
            })
            post.success(function (data) {
                $scope.books = data;
            });

            post.error(function (data) {
                $window.alert(data.Message);
            });
        }
        

        $scope.AddBooks = function () {

            let book = 
            {title: $scope.title,
            categoryName: $scope.category,
            authorName: $scope.author,
            publisherName: $scope.publisher,
            publishDate: new Date ($scope.publishDate)};

            var post = $http({
                method: "POST",
                url: "http://localhost:80/book/",
                dataType: 'json',
                data: book,
                headers: { 
                    "Content-Type": "application/json" }
            })
            .then(function (response) {
                toastr.success("Successfully Added");
                $scope.repos = response.data;
                $scope.GetBooks();
                $scope.id = null;
                $scope.title = null;
                $scope.category = null;
                $scope.author = null;
                $scope.publisher = null;
                $scope.publishDate = null;
            });
        };

        $scope.DeleteBooks = function () {

            $scope.showconfirmbox();
            
           if($scope.result == "Yes"){
            let book = 
            {id: $scope.id,};

            var post = $http({
                method: "DELETE",
                url: "http://localhost:80/book/"+ $scope.id,
                dataType: 'json',
                data: book,
                headers: { 
                    "Content-Type": "application/json" }
            }).finally(function (response) {
                $scope.GetBooks();
                toastr.success("Successfully Deleted");
                $scope.id = null;
                $scope.title = null;
                $scope.category = null;
                $scope.author = null;
                $scope.publisher = null;
                $scope.publishDate = null;

                $scope.isDisabled = false;
             });
           }
            
        };

        $scope.UpdateBooks = function () {
            
            let book = 
            {
            id:$scope.id,
            title: $scope.title,
            categoryName: $scope.category,
            authorName: $scope.author,
            publisherName: $scope.publisher,
            publishDate: new Date($scope.publishDate)
          };

            var post = $http({
                method: "PUT",
                url: "http://localhost:80/book/"+ book.id ,
                dataType: 'json',
                data: book,
                headers: { 
                    "Content-Type": "application/json" }
            })
            .then(function (response) {
                toastr.success("Successfully Updated");
                $scope.repos = response.data;
                $scope.GetBooks();
                $scope.id = null;
                $scope.title = null;
                $scope.category = null;
                $scope.author = null;
                $scope.publisher = null;
                $scope.publishDate = null;

                $scope.isDisabled = false;
            });
        };

        $scope.GetAuthors = function () {   
            var post = $http({
                    method: "GET",
                    url: "http://localhost:80/book/authors",
                    dataType: 'json',
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (data) {
                    $scope.authors = data;
                    var select = document.getElementById("author");
                    for(var i = 0; i < data.data.length; i++) {
                        var opt = data.data[i].authorName;
                        var el = document.createElement("option");
                        el.textContent = opt;
                        el.value = opt;
                        select.appendChild(el);
                   }
                   $scope.GetBooks();
                });
            };

            $scope.Authors = function () {   
                var post = $http({
                        method: "GET",
                        url: "http://localhost:80/book/authors",
                        dataType: 'json',
                        headers: { 
                            "Content-Type": "application/json" }
                    })
                    .then(function (data) {
                        $scope.a = data;
                    });
                };

        $scope.GetCategories = function () {   
            var post = $http({
                    method: "GET",
                    url: "http://localhost:80/book/categories",
                    dataType: 'json',
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (data) {
                    $scope.categories = data;
                    var select = document.getElementById("category");
                    for(var i = 0; i < data.data.length; i++) {
                        var opt = data.data[i].categoryName;
                        var el = document.createElement("option");
                        el.textContent = opt;
                        el.value = opt;
                        select.appendChild(el);
                   }
                   $scope.GetBooks();
                });
            };

            $scope.GetPublishers = function () {   
                var post = $http({
                        method: "GET",
                        url: "http://localhost:80/book/publishers",
                        dataType: 'json',
                        headers: { 
                            "Content-Type": "application/json" }
                    })
                    .then(function (data) {
                        $scope.books = data;
                        var select = document.getElementById("publisher");
                        for(var i = 0; i < data.data.length; i++) {
                            var opt = data.data[i].publisherName;
                            var el = document.createElement("option");
                            el.textContent = opt;
                            el.value = opt;
                            select.appendChild(el);
                       }
                       $scope.GetBooks();
                    });
                };

        $scope.select = function(e) {

            var table = document.getElementsByTagName("table")[0];
            var tbody = table.getElementsByTagName("tbody")[0];
            let data = [];
            tbody.onclick = function (e) {
            e = e || window.event;
            target = e.srcElement || e.target; 
            while (target && target.nodeName !== "TR") {
            target = target.parentNode;
            }
            
            if (target) {
            var cells = target.getElementsByTagName("td");
            for (var i = 0; i < cells.length; i++) {
            data.push(cells[i].innerHTML);
            $scope.isDisabled = true;
           } 
           $scope.id = data[0];
           $scope.title = data[1];
           $scope.category = data[2];
           $scope.author = data[3];
           $scope.publisher = data[4];
           $scope.publishDate = data[5];
          }
        };
     }

     $scope.showconfirmbox = function () {
        if ($window.confirm("Are you Sure?"))
        $scope.result = "Yes";
        else
        $scope.result = "No";
        }
    
        $scope.GetBooks();
        $scope.GetCategories();
        $scope.GetPublishers();
        $scope.GetAuthors();
        $scope.Authors();
});