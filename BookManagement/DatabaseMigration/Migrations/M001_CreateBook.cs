﻿using FluentMigrator;

namespace DatabaseMigration.Database
{
    [Migration(1)]
    public class M001_CreateBook : Migration
    {
        public override void Up()
        {
            Create.Table("Book")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable().Indexed()
                .WithColumn("Title").AsString(250).NotNullable()
                .WithColumn("Author").AsString(250).NotNullable()
                .WithColumn("Category").AsString(250).Nullable()
                .WithColumn("Publisher").AsString(250).NotNullable()
                .WithColumn("PublishDate").AsDateTime().Nullable();
        }
        public override void Down()
        {
              Delete.Table("Book");
        }
    }
}
