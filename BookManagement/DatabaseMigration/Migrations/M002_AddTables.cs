﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace DatabaseMigration.Migrations
{
    [Migration(2)]
    public class M002_AddTables : Migration
    {
        public override void Up()
        {

            Create.Table("Author")
                .WithColumn("AuthorId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("AuthorName").AsString(250).NotNullable()
                .WithColumn("DateOfBirth").AsDateTime().Nullable();

            Create.Table("Publisher")
                .WithColumn("PublisherId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("PublisherName").AsString(250).NotNullable();

            Create.Table("Category")
                .WithColumn("CategoryId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("CategoryName").AsString(250).NotNullable();

            Create.Table("User")
                .WithColumn("UserId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("UserName").AsString(250).NotNullable();

            Create.Table("Book")
                .WithColumn("BookId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("Title").AsString(250).NotNullable()
                .WithColumn("AuthorId").AsInt32().ForeignKey("Author", "AuthorId").NotNullable()
                .WithColumn("CategoryId").AsInt32().ForeignKey("Category", "CategoryId").NotNullable()
                .WithColumn("PublisherId").AsInt32().ForeignKey("Publisher", "PublisherId").NotNullable()
                .WithColumn("PublishDate").AsDateTime().Nullable();


            Insert.IntoTable("Publisher").Row(new { PublisherName = "SA Home Loans"})
                                                  .Row(new { PublisherName = "Adapt IT" })
                                                  .Row(new { PublisherName = "Derivco" })
                                                  .Row(new { PublisherName = "Momentum" })
                                                  .Row(new { PublisherName = "SA Home Loans"});

            Insert.IntoTable("Author") .Row(new { AuthorName = "Mark brown" })
                                                .Row(new { AuthorName = "Zandile Mbatha" })
                                                .Row(new { AuthorName = "Lean White" })
                                                .Row(new { AuthorName = "Lee Moon" })
                                                .Row(new { AuthorName = "Chris rock" });

            Insert.IntoTable("Category").Row(new { CategoryName = "Music" })
                                                .Row(new { CategoryName = "Cartoons" })
                                                .Row(new { CategoryName = "Education" })
                                                .Row(new { CategoryName = "Comedy" })
                                                .Row(new { CategoryName = "History" });

        }
        public override void Down()
        {
            Delete.Table("Book");
            Delete.Table("Author");
            Delete.Table("Publisher");
            Delete.Table("Category");
            Delete.Table("User");
        }
    }
}
