﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementApi.Interfaces;
using BookManagementApi.Mappers;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

namespace BookManagementApi
{
    public class BootStrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register(typeof(IBookRepository), typeof(BookRepository));
            pipelines.AfterRequest += (ctx) => 
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                        .WithHeader("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE")
                        .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
            };
         }
    }
}
