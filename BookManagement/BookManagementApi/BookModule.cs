﻿using BookManagementApi.Interfaces;
using BookManagementApi.Models;
using Nancy;
using Nancy.ModelBinding;

namespace BookManagementApi
{
    public class BookModule : NancyModule
    {
        private readonly IBookRepository _bookRepository;

        public BookModule(IBookRepository bookRepository) : base("/book")
        {
            _bookRepository = bookRepository;

            GetAllBooks();

            GetBookById();

            AddBook();

            Put();

            Delete();

            GetCategories();

            GetPublishers();

            GetAuthors();

            AddAuthor();

            DeleteAuthor();

            UpdateAuthor();

            AddCategory();

            DeleteCategory();

            UpdateCategory();

            AddPublisher();

            DeletePublisher();

            UpdatePublisher();
        }

        public void AddBook()
        {
            Post("/", parameters =>
            {
                var model = this.Bind<BookModel>();
                _bookRepository.Add(model);
                return Negotiate.WithModel(model)
                    .WithStatusCode(HttpStatusCode.Created);
            });
        }

        public void GetBookById()
        {
            Get("/{id}", parameters =>
            {
                return _bookRepository.GetByID(parameters.Id);

            });
        }

        public void GetAllBooks()
        {
            Get("/", parameters =>
            {
                return _bookRepository.GetAll();

            });
        }

        public void Delete()
        {
            Delete("/{id}", parameters =>
            {
                _bookRepository.Delete(parameters.Id);
                return HttpStatusCode.OK;
            });
        }

        public void Put()
        {
            Put("/{bookId}", parameters =>
            {
                var model = this.Bind<BookModel>();
                _bookRepository.Update(model);
                return model;
            });
        }

        public void GetCategories()
        {
            Get("/categories", parameters =>
            {
                return _bookRepository.GetCategories();

            });
        }

        public void GetPublishers()
        {
            Get("/publishers", parameters =>
            {
                return _bookRepository.GetPublishers();

            });
        }   

        public void GetAuthors()
        {
            Get("/authors", parameters =>
            {
                return _bookRepository.GetAuthors();

            });
        }

        public void AddAuthor()
        {
            Post("/author", parameters => 
            {
                var model = this.Bind<AuthorModel>();
                _bookRepository.AddAuthor(model);
                return Negotiate.WithModel(model)
                    .WithStatusCode(HttpStatusCode.Created);
            });
        }

        public void DeleteAuthor()
        {
            Delete("/author/{id}", parameters =>
            {
                _bookRepository.DeleteAuthor(parameters.Id);
                return HttpStatusCode.OK;
            });
        }

        public void UpdateAuthor()  
        {
            Put("/author/{authorId}", parameters =>
            {
                var model = this.Bind<AuthorModel>();
                _bookRepository.UpdateAuthor(model);
                return model;
            });
        }

        public void AddCategory()
        {
            Post("/category", parameters =>
            {
                var model = this.Bind<CategoryModel>();
                _bookRepository.AddCategory(model);
                return Negotiate.WithModel(model)
                    .WithStatusCode(HttpStatusCode.Created);
            });
        }

        public void DeleteCategory()
        {
            Delete("/category/{id}", parameters =>
            {
                _bookRepository.DeleteCategory(parameters.Id);
                return HttpStatusCode.OK;
            });
        }

        public void UpdateCategory()
        {
            Put("/category/{categoryId}", parameters =>
            {   
                var model = this.Bind<CategoryModel>();
                _bookRepository.UpdateCategory(model);
                return model;
            });
        }
            
        public void AddPublisher()
        {
            Post("/publisher", parameters =>
            {
                var model = this.Bind<PublisherModel>();
                _bookRepository.AddPublisher(model);
                return Negotiate.WithModel(model)
                    .WithStatusCode(HttpStatusCode.Created);
            });
        }
            
        public void DeletePublisher()
        {
            Delete("/publisher/{id}", parameters =>
            {
                _bookRepository.DeletePublisher(parameters.Id);
                return HttpStatusCode.OK;
            });
        }
            
        public void UpdatePublisher()
        {
            Put("/publisher/{publisherId}", parameters =>
            {
                var model = this.Bind<PublisherModel>();
                _bookRepository.UpdatePublisher(model);
                return model;
            });
        }
    }
}
