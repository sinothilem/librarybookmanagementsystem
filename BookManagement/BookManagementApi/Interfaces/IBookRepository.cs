﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagementApi.Models;

namespace BookManagementApi.Interfaces
{
    public interface IBookRepository
    {
         void Add(BookModel book);
         IEnumerable<BookModel> GetAll();
         BookModel GetByID(int id);
         void Delete(int id);
         void Update(BookModel book);
         IEnumerable<CategoryModel> GetCategories();    
         IEnumerable<AuthorModel> GetAuthors();
         IEnumerable<PublisherModel> GetPublishers();
         void AddAuthor(AuthorModel book);
         void DeleteAuthor(int id);
         void UpdateAuthor(AuthorModel author);
         void AddCategory(CategoryModel book);
         void DeleteCategory(int id);
         void UpdateCategory(CategoryModel author);
         void AddPublisher(PublisherModel book);
         void DeletePublisher(int id);
         void UpdatePublisher(PublisherModel author);
    }
}
