﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy.Hosting.Self;

namespace BookManagementApi
{
   public class Program
    {
       public static void Main(string[] args)
        {
            using (var host = new NancyHost(new Uri("http://localhost:80")))
            {
                host.Start();

                Console.WriteLine("NancyFX Stand alone application.");
                Console.WriteLine("Press enter to exit the application");
                Console.ReadLine();
            }
        }
    }
}
