﻿using System;

namespace BookManagementApi
{
    public class BookModel
    {
        public int BookId { get; set; }
        public int AuthorId { get; set; }
        public int PublisherId { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }   
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }
        public string CategoryName { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime DateOfBirth { get; set; }

    }
}