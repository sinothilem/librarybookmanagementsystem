﻿using BookManagementApi.Mappers;
using BookManagementApiTests.Builders;
using Nancy;
using Nancy.Testing;
using NUnit.Framework;

namespace BookManagementIntegrationTests
{
    [TestFixture]
    public class BookManagerApiTest
    {
        [Test]
        public void Should_return_status_ok_when_route_exists()
        {
            //Arrange
            var bookRepository = CreateBookRepository();

            //Act
            var result = new EndpointBuilder(bookRepository)
                        .GetAll()
                        .Build();

            //Assert
            Assert.AreEqual(HttpStatusCode.OK, result.Result.StatusCode);
        }

        [Test]
        public void When_Data_Available_Should_Get_Data_And_Status_Should_Be_OK()
        {
            // Arrange
            var bookRepository = CreateBookRepository();

            // Act  
            var response = new EndpointBuilder(bookRepository)
                          .GetAll()
                          .Build();

            var actual = response.Result.Body.AsString();

            // Assert
            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(HttpStatusCode.OK, response.Result.StatusCode);
        }

        [Test]
        public void When_Data_Available_Should_Get_Data_By_Id_And_Status_Should_Be_OK()
        {
            // Arrange      
            var id = 87;
            var bookRepository = CreateBookRepository();

            // Act  
            var response = new EndpointBuilder(bookRepository)
                          .GetById(id)
                          .Build();

            var actual = response.Result.Body.AsString();

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.Result.StatusCode);
        }

        [Test]
        public void When_Posting_Should_Insert_Data_In_Database_And_Status_Should_Be_Created()
        {
            // Arrange
            var bookRepository = CreateBookRepository();
            var request = new BookManagerBuilder().WithBook().Build();

            // Act  
            var response = new EndpointBuilder(bookRepository)
                          .Post(request)
                          .Build();

            var actual = response.Result.Body.AsString();

            // Assert
            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(HttpStatusCode.Created, response.Result.StatusCode);
        }

        [Test]
        public void When_Updating_Should_Update_Data_In_Database_by_Id_And_Status_Should_Be_OK()
        {
            // Arrange
            var id = 87;
            var bookRepository = CreateBookRepository();
            var request = new BookManagerBuilder().WithBook().Build();

            // Act  
            var response = new EndpointBuilder(bookRepository)
                          .Put(id, request)
                          .Build();

            var actual = response.Result.Body.AsString();

            // Assert
            Assert.That(actual, Is.Not.Null);
            Assert.AreEqual(HttpStatusCode.OK, response.Result.StatusCode);
        }

        [Test]
        public void When_Deleting_Should_Delete_Data_In_Database_by_Id_And_Status_Should_Be_OK()
        {
            // Arrange
            var id = 1;
            var bookRepository = CreateBookRepository();

            // Act  
            var response = new EndpointBuilder(bookRepository)
                          .Delete(id)
                          .Build();

            var actual = response.Result.Body.AsString();

            // Assert
            Assert.That(actual, Is.Empty);
            Assert.AreEqual(HttpStatusCode.OK, response.Result.StatusCode);
        }

        private static BookRepository CreateBookRepository()
        {
            return new BookRepository();
        }
    }
}
