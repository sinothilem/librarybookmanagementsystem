﻿using System;
using BookManagementApi.Mappers;
using System.Linq;
using BookManagementApi;
using BookManagementApiTests.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace BookManagementIntegrationTests
{
    [TestFixture]
    public class BookRepositoryTests
    {
        [Test]
        public void Should_Get_All_Books_From_Database()
        {
            //Arrange
            var _bookRepository = CreateBookRepository();

            //Act
            var actual = _bookRepository.GetAll();

            //Assert
            actual.Count().Should().BeGreaterThan(0);
        }

        [Test]
        public void Should_Get_Book_By_Id_From_Database()
        {
            //Arrange
            var id = 2;
            var _bookRepository = CreateBookRepository();

            //Act
            var actual = _bookRepository.GetByID(id);

            //Assert
            Assert.That(actual, Is.Not.Null);
        }

        [Test]
        public void Should_Insert_Book_In_Database()
        {
            //Arrange
            var book = new BookManagerBuilder()
                        .WithBook()
                        .Build();

            var _bookRepository = CreateBookRepository();

            //Act
            _bookRepository.Add(book);
            var actual = _bookRepository.GetAll();

            //Assert
            var books = actual.LastOrDefault();
            Assert.That(books, Is.Not.Null);
        }

        [Test]
        public void Should_Update_A_Book_Title_In_Database()
        {
            //Arrange
            var book = new BookManagerBuilder()
                        .WithBook()
                        .Build();

            var _bookRepository = CreateBookRepository();

            //Act
            _bookRepository.Update(book);
            var actual = _bookRepository.GetByID(2);

            //Assert
            Assert.That(actual, Is.Not.Null);
        }

        [Test]
        public void Should_Delete_A_Book_In_Database()
        {
            //Arrange
            var id = 3;
            var _bookRepository = CreateBookRepository();

            //Act
            _bookRepository.Delete(id);
            var actual = _bookRepository.GetByID(id);

            //Assert
            Assert.That(actual, Is.Null);
        }

        private static BookRepository CreateBookRepository()
        {
            return new BookRepository();
        }
    }
}
